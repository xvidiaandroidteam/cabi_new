package global.xvidia.net.googlemapdirections.network;

import global.xvidia.net.googlemapdirections.MyApplication;
import global.xvidia.net.googlemapdirections.R;

public class ServiceURLManager {

    private static ServiceURLManager instance = null;

    private ServiceURLManager() {

    }

    public static ServiceURLManager getInstance() {
        if (instance == null) {
            instance = new ServiceURLManager();
        }
        return instance;
    }

    final String base_URL = MyApplication.getAppContext().getResources().getString(R.string.base_url);

//    final String base_URL_Live= MyApplication.getAppContext().getResources().getString(R.string.base_url_live);

    public String getLoginUrl() {
        String url = base_URL + "passenger/login";
        return url;
    }
    public String getRegisterUrl() {
        String url = base_URL + "passenger";
        return url;
    }
    public String getBookCabUrl(String number) {
        String url = base_URL + "bookMyTrip/" + number;
        return url;
    }

    public String getLiveSubscriberTokenUrl(String regNo) {
        String url = base_URL + "getCarByRegNo/"+regNo;
        url = url.replace("+", "%2B");
        return url;
    }

    public String getCloseLiveUrl(String sessionId) {
        String url = base_URL + sessionId;
        url = url.replace("+","%2B");
        return url;
    }
    public String getStartTripUrl() {
        String url = base_URL + "startTrip";
        url = url.replace("+","%2B");
        return url;
    }

    public String getStopTripUrl() {
        String url = base_URL + "stopTrip";
        url = url.replace("+","%2B");
        return url;
    }

    public String getSaveEmergencyContact() {
        String url = base_URL + "emergencyContact";
        url = url.replace("+","%2B");
        return url;
    }

    public String getEmergencyContact(String username) {
        String url = base_URL + "getEmergencyContact/"+username;
        url = url.replace("+","%2B");
        return url;
    }
    public String getOtpDetail(String otp) {
        String url = base_URL + "device/"+otp;
        url = url.replace("+","%2B");
        return url;
    }
    public String getFindBuddyUrl() {
        String url = base_URL + "device/";
        url = url.replace("+","%2B");
        return url;
    }
    public String getDeviceIdUrl(String deviceId, String strId) {
        String url = base_URL + "device/getDevice/"+ deviceId + "/"+ strId;
        url = url.replace("+","%2B");
        return url;
    }
}
