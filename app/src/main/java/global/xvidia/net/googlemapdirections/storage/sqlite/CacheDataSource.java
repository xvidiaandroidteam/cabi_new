package global.xvidia.net.googlemapdirections.storage.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import global.xvidia.net.googlemapdirections.network.model.ContactData;

public class CacheDataSource {
	private SQLiteDatabase database;
	private final MySqliteHelper dbHelper;
	private static CacheDataSource INSTANCE = null;// new CachDataSource();

	public static CacheDataSource getInstance() {
		if (INSTANCE == null)
			INSTANCE = new CacheDataSource();
		return INSTANCE;
	}

	private CacheDataSource() {
		dbHelper = MySqliteHelper.getInstance();
	}

	public void open(Context c) throws SQLException {
		try {
			if (database == null) {
				database = dbHelper.getWritableDatabase();
			} else if (!database.isOpen()) {
				database = dbHelper.getWritableDatabase();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void close() {
		// Log.d("CachedDataSource", "Closed");
		dbHelper.close();
		database.close();
	}


	/*
	 * Facebook DATA
	 */

	public void saveContactData(ContactData settingObj) {
		try {
			if(settingObj == null)
				return;
			ContentValues values = new ContentValues();
			values.put(MySqliteHelper.COLUMN_CONTACT_ID,
					settingObj.getContactId());
			values.put(MySqliteHelper.COLUMN_CONTACT_DISPLAY_NAME,
					settingObj.getContactName());
			values.put(MySqliteHelper.COLUMN_CONTACT_PHONENUMBER,
					settingObj.getMobileNumber());
			if (getPhoneData(settingObj.getContactId()) != null) {
				updateContactData(settingObj);
			}else {
				database.insert(MySqliteHelper.TABLE_NAME_CONTACTS, null, values);
			}
		} catch (Exception e) {
		}
	}

	public void removeContactData() {
		int log = database.delete(MySqliteHelper.TABLE_NAME_CONTACTS, null, null);
		Log.i("deleted",""+log);
	}

	public void updateContactData(ContactData settingObj) {
		try {
			if(settingObj == null)
				return ;
			ContentValues values = new ContentValues();
			values.put(MySqliteHelper.COLUMN_CONTACT_ID,
					settingObj.getContactId());
			values.put(MySqliteHelper.COLUMN_CONTACT_DISPLAY_NAME,
					settingObj.getContactName());
			values.put(MySqliteHelper.COLUMN_CONTACT_PHONENUMBER,
					settingObj.getMobileNumber());
			database.update(MySqliteHelper.TABLE_NAME_CONTACTS, values,
					MySqliteHelper.COLUMN_CONTACT_ID+ "=?",
					new String[]{settingObj.getContactId()});
		} catch (Exception e) {
		}
	}

	public ContactData getPhoneData(String phoneNumber) {
		ContactData data = null;

		Cursor cursor = null;
		try {
			cursor = database.query(MySqliteHelper.TABLE_NAME_CONTACTS, null,
					MySqliteHelper.COLUMN_CONTACT_ID + "=?",
					new String[]{phoneNumber}, null, null, null);

			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				data = new ContactData();
				data.setContactName(cursor.getString(cursor
						.getColumnIndex(MySqliteHelper.COLUMN_CONTACT_DISPLAY_NAME)));
				data.setMobileNumber(cursor.getString(cursor
						.getColumnIndex(MySqliteHelper.COLUMN_CONTACT_PHONENUMBER)));
			}
			if (cursor != null){
				cursor.close();
			}
		} catch (Exception e) {
			if (cursor != null)
				cursor.close();
		}
		return data;
	}
	public synchronized ArrayList<ContactData> getContactlistData() {
		// String data = null;
		ArrayList<ContactData> contactDataObj = null;
		Cursor cursor = null;

		try {

			contactDataObj = new ArrayList<>();
			cursor = database.query(MySqliteHelper.TABLE_NAME_CONTACTS, null,
					null, null, null, null, null);

			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				for (int i = cursor.getPosition(); i < cursor.getCount(); i++) {
					cursor.moveToPosition(i);
					ContactData contactData;
					contactData = new ContactData();
					contactData.setContactId(cursor.getString(cursor
							.getColumnIndex(MySqliteHelper.COLUMN_CONTACT_ID)));
					contactData.setContactName(cursor.getString(cursor
							.getColumnIndex(MySqliteHelper.COLUMN_CONTACT_DISPLAY_NAME)));
					contactData.setMobileNumber(cursor.getString(cursor
							.getColumnIndex(MySqliteHelper.COLUMN_CONTACT_PHONENUMBER)));
					contactDataObj.add(contactData);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (cursor != null)
			cursor.close();
		return contactDataObj;
	}
}
