package global.xvidia.net.googlemapdirections;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import global.xvidia.net.googlemapdirections.network.ServiceURLManager;
import global.xvidia.net.googlemapdirections.network.VolleySingleton;
import global.xvidia.net.googlemapdirections.network.model.ContactData;
import global.xvidia.net.googlemapdirections.network.model.EmergencyContactDetails;
import global.xvidia.net.googlemapdirections.storage.DataStorage;
import global.xvidia.net.googlemapdirections.storage.sqlite.manager.DataCacheManager;

/**
 * A login screen that offers login via email/password.
 */
public class EmergencyContactActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_PERMISSIONS1 = 121;
    public static final int REQUEST_CODE_PERMISSIONS2 = 122;
    public static final int REQUEST_CODE_PERMISSIONS3 = 123;
    public static final int REQUEST_CODE_PERMISSIONS4 = 124;
    public static final int REQUEST_CODE_PERMISSIONS5 = 125;
    public static final int REQUEST_CODE_ASK_PERMISSIONS = 127;
    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 126;
    // UI references.
    private TextView mNumberView1, mNameView1;
    private TextView mNumberView2, mNameView2;
    private TextView mNumberView3, mNameView3;
    private TextView mNumberView4, mNameView4;
    private TextView mNumberView5, mNameView5;
    private Button saveButton;
    static int permissionCode = 0;
    private ImageButton mContactButton1, mContactButton2, mContactButton3, mContactButton4, mContactButton5;
    private ProgressBar progressBar;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact);
        activity = this;
        mNameView1 = (TextView) findViewById(R.id.contactName1);
        mNameView2 = (TextView) findViewById(R.id.contactName2);
        mNameView3 = (TextView) findViewById(R.id.contactName3);
        mNameView4 = (TextView) findViewById(R.id.contactName4);
        mNameView5 = (TextView) findViewById(R.id.contactName5);
        mNumberView1 = (TextView) findViewById(R.id.contact_phone1);
        mNumberView2 = (TextView) findViewById(R.id.contact_phone2);
        mNumberView3 = (TextView) findViewById(R.id.contact_phone3);
        mNumberView4 = (TextView) findViewById(R.id.contact_phone4);
        mNumberView5 = (TextView) findViewById(R.id.contact_phone5);
        mContactButton1 = (ImageButton) findViewById(R.id.contactPhoneButton1);
        mContactButton2 = (ImageButton) findViewById(R.id.contactPhoneButton2);
        mContactButton3 = (ImageButton) findViewById(R.id.contactPhoneButton3);
        mContactButton4 = (ImageButton) findViewById(R.id.contactPhoneButton4);
        mContactButton5 = (ImageButton) findViewById(R.id.contactPhoneButton5);
        saveButton = (Button) findViewById(R.id.contactSave);
        saveButton.setVisibility(View.GONE);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        saveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                checkSmsPermission();
            }
        });
        mContactButton1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPerm(REQUEST_CODE_PERMISSIONS1);
            }
        });

        mContactButton2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                checkPerm(REQUEST_CODE_PERMISSIONS2);
            }
        });
        mContactButton3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPerm(REQUEST_CODE_PERMISSIONS3);

            }
        });
        mContactButton4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPerm(REQUEST_CODE_PERMISSIONS4);
            }
        });
        mContactButton5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPerm(REQUEST_CODE_PERMISSIONS5);
            }
        });
        showProgress(false);
        initialiseContacts();
        bindActivity();
        ArrayList<ContactData> contactList = new ArrayList<>();
        contactList = DataCacheManager.getInstance().getContactlistData();
        if (contactList != null && contactList.size() > 0) {
            saveButton.setVisibility(View.VISIBLE);
        }
    }

    private void checkSmsPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_SMS))
            permissionsNeeded.add("Send SMS");

//        final String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS,Manifest.permission.READ_SMS};

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
            saveContacts();
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
//                    String message = "You need to grant access to Contacts and SMS permissions";
                    String message = "You need to grant access to  " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;

            }
        }

    }

    private void bindActivity() {

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setTitle(getString(R.string.em_contacts));
        mToolbar.setPadding(0, 0, 0, 0);
//        getProfileRequest(false);
//        getVideoUrls();
    }

    private void checkPerm(int code) {
        permissionCode = code;
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_CONTACTS))
            permissionsNeeded.add("Read Contacts");

//        final String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS,Manifest.permission.READ_SMS};

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
            attemptLogin(code);
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
//                    String message = "You need to grant access to Contacts and SMS permissions";
                    String message = "You need to grant access to  " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                        showMessageOKCancel(message,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                    }
                                });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
            // Initial
            perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);

            // Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for ACCESS_FINE_LOCATION
            if (perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                attemptLogin(permissionCode);
            } else {
                Toast.makeText(getApplicationContext(), "Permissions has not been granted", Toast.LENGTH_LONG).show();

            }
        }else if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
            // Initial
            perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);

            // Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for ACCESS_FINE_LOCATION
            if (perms.get(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
               saveContacts();
            } else {
                Toast.makeText(getApplicationContext(), "Permissions has not been granted", Toast.LENGTH_LONG).show();

            }
        }  else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(EmergencyContactActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin(int code) {

        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, code);
    }


    private void contactPicked(Intent data, int position) {
        Cursor cursor = null;
        try {
            String phoneNo = null;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            ContactData dataContact = new ContactData();
            dataContact.setContactId(String.valueOf(position));
            dataContact.setMobileNumber(phoneNo);
            dataContact.setContactName(name);
            DataCacheManager.getInstance().saveContactData(dataContact);
            if (position == 1) {
                mNameView1.setText(dataContact.getContactName());
                mNumberView1.setText(dataContact.getMobileNumber());
            } else if (position == 2) {
                mNameView2.setText(dataContact.getContactName());
                mNumberView2.setText(dataContact.getMobileNumber());
            } else if (position == 3) {
                mNameView3.setText(dataContact.getContactName());
                mNumberView3.setText(dataContact.getMobileNumber());
            } else if (position == 4) {
                mNameView4.setText(dataContact.getContactName());
                mNumberView4.setText(dataContact.getMobileNumber());
            } else if (position == 5) {
                mNameView5.setText(dataContact.getContactName());
                mNumberView5.setText(dataContact.getMobileNumber());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check whether the result is ok
        if (resultCode == RESULT_OK) {
            // Check for the request code, we might be usign multiple startActivityForReslut
            switch (requestCode) {
                case REQUEST_CODE_PERMISSIONS1:
                    contactPicked(data, 1);
                    break;
                case REQUEST_CODE_PERMISSIONS2:
                    contactPicked(data, 2);
                    break;
                case REQUEST_CODE_PERMISSIONS3:
                    contactPicked(data, 3);
                    break;
                case REQUEST_CODE_PERMISSIONS4:
                    contactPicked(data, 4);
                    break;
                case REQUEST_CODE_PERMISSIONS5:
                    contactPicked(data, 5);
                    break;

            }
        } else {
            Log.e("Emergency contact", "Failed to pick contact");
        }
    }

    private void initialiseContacts() {
        ArrayList<ContactData> contactList = new ArrayList<>();
        contactList = DataCacheManager.getInstance().getContactlistData();
        if (contactList != null && contactList.size() > 0) {
            for (int i = 0; i < contactList.size(); i++) {
                if (i == 0) {
                    mNameView1.setText(contactList.get(i).getContactName());
                    mNumberView1.setText(contactList.get(i).getMobileNumber());
                } else if (i == 1) {
                    mNameView2.setText(contactList.get(i).getContactName());
                    mNumberView2.setText(contactList.get(i).getMobileNumber());
                } else if (i == 2) {
                    mNameView3.setText(contactList.get(i).getContactName());
                    mNumberView3.setText(contactList.get(i).getMobileNumber());
                } else if (i == 3) {
                    mNameView4.setText(contactList.get(i).getContactName());
                    mNumberView4.setText(contactList.get(i).getMobileNumber());
                } else if (i == 4) {
                    mNameView5.setText(contactList.get(i).getContactName());
                    mNumberView5.setText(contactList.get(i).getMobileNumber());
                }
            }
        }
    }

    private void saveContacts() {
        ArrayList<ContactData> contactList = new ArrayList<>();
        contactList = DataCacheManager.getInstance().getContactlistData();
        if (contactList != null && contactList.size() > 0) {
            String url = ServiceURLManager.getInstance().getSaveEmergencyContact();
            EmergencyContactDetails obj = new EmergencyContactDetails();
            obj.setPassengerMobileNo(DataStorage.getInstance().getUsername());
            obj.setEmergencyContactList(contactList);
            try {
                ObjectMapper mapper = new ObjectMapper();
                String jsonObject = null;
                try {
                    jsonObject = mapper.writeValueAsString(obj);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Toast.makeText(activity, "Contacts saved successfully", Toast.LENGTH_LONG).show();

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("response", error.toString());
                        showProgress(false);

                        showError(getString(R.string.error_general), null);
                    }
                });
                request.setRetryPolicy(new DefaultRetryPolicy(
                        30000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
                VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}

            } catch (Exception e) {
                e.printStackTrace();

            }
        } else {
            showError(getString(R.string.error_emergency_contact_add), null);
        }
    }

    private void showProgress(final boolean show) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Animation rotation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.rotate);
//                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
//                    mProgressView.startAnimation(rotation);
                    progressBar.setVisibility(View.VISIBLE);
                } else {
//                    mProgressView.clearAnimation();
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    private void showError(String message, DialogInterface.OnClickListener okClicked) {
        new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, okClicked)
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ArrayList<ContactData> contactList = new ArrayList<>();
        contactList = DataCacheManager.getInstance().getContactlistData();
        if (contactList != null && contactList.size() > 0) {
            saveButton.setVisibility(View.VISIBLE);
        }
    }
}

