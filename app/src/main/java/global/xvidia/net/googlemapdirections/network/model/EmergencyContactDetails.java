package global.xvidia.net.googlemapdirections.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by vasu on 6/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmergencyContactDetails {
    @JsonProperty("passengerMobileNo")
    private String passengerMobileNo;
    @JsonProperty("emergencyContactList")
    private List<ContactData> emergencyContactList;

    public String getPassengerMobileNo() {
        return passengerMobileNo;
    }

    public void setPassengerMobileNo(String passengerMobileNo) {
        this.passengerMobileNo = passengerMobileNo;
    }

    public List<ContactData> getEmergencyContactList() {
        return emergencyContactList;
    }

    public void setEmergencyContactList(List<ContactData> emergencyContactList) {
        this.emergencyContactList = emergencyContactList;
    }
}
